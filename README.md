# Seafile docker image for FLAP

---

#### Cloning

It is preferable to clone the main project:

`git clone --recursive git@gitlab.com:flap-box/flap.git`

We need the `--recursive` flag so submodules are also cloned.

#### Todos

-   [ ] Fail2ban
-   [ ] Cron job (GC)
-   [ ] Redirect login page

#### More todos

-   [ ] Track release of https://github.com/haiwen/seahub/pull/2925 and remove `patch.sso` from the docker build
-   [ ] Theme
-   [ ] Onlyoffice
