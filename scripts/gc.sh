#!/bin/bash

# Launch the garbage collection script in seafile
# Launching it once a week should be enough
docker-compose exec seafile /shared/seafile-server-latest/seafile.sh stop
docker-compose exec seafile /shared/seafile-server-latest/seaf-gc.sh
docker-compose exec seafile /shared/seafile-server-latest/seafile.sh start
